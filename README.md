Role Name
=========

This role sets up a loopdevice for testing storage related tasks.

Requirements
------------

  

Role Variables
--------------

```
disk_name: vd01
disk_size: 1024
destroy_loopdevice: false
```

Dependencies
------------

None

Example Playbook
----------------

```
- hosts: backup_clients
  roles:
     -  role: ansible_role_loopdevice
        
```

License
-------

BSD

Author Information
------------------

The Development Range Engineering, Architecture, and Modernization (DREAM) Team
