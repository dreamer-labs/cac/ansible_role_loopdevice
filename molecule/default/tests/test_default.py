import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_borgbackup(host):
    ps = host.run("losetup --list | grep vd01 | awk '{ print $1  }'")
    ps.rc
